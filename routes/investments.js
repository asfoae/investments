const express = require('express');
const router = express.Router();

const Investment = require('../models/Investment');

// Get all investments
router.get('/', async (req, res) => {
  try {
    const investments = await Investment.find().populate('category');;
    res.json(investments);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Create a new investment
router.post('/', async (req, res) => {
  const investment = new Investment(req.body);

  try {
    const newInvestment = await investment.save();
    res.status(201).json(newInvestment);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Get a specific investment by ID
router.get('/:id', getInvestmentById, (req, res) => {
  res.json(res.investment);
});

// Update a specific investment by ID
router.put('/:id', getInvestmentById, async (req, res) => {
  try {
    Object.assign(res.investment, req.body);
    const updatedInvestment = await res.investment.save();
    res.json(updatedInvestment);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a specific investment by ID
router.delete('/:id', getInvestmentById, async (req, res) => {
  try {
    await res.investment.remove();
    res.json({ message: 'Investment deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Middleware to find an investment by ID
async function getInvestmentById(req, res, next) {
  let investment;
  try {
    investment = await Investment.findById(req.params.id).populate('category');
    if (!investment) {
      return res.status(404).json({ message: 'Investment not found' });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }

  res.investment = investment;
  next();
}

module.exports = router;