const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const connectDB = require('./config/database');

// Connect to MongoDB
connectDB();

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

// Routes
app.get('/', (req, res) => {
  res.json({ message: 'Investments API' });
});

const investmentsRouter = require('./routes/investments');

app.use('/investments', investmentsRouter);

// Start the server
const PORT = process.env.PORT || 3003;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
